import React from "react";
import { Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";
import { useAuth0 } from "@auth0/auth0-react";

import { NavBar, Footer, Loading } from "./components";
import { Home, Profile, ExternalApi } from "./views";
import NavLoggedIn from './components/nav-barloggedin'
import NavLoggedOut from './components/nav-barloggedout'
import Dashboard  from './components/dashboard'
import ManagementPage from './views/management'

import "./app.css";

const App = () => {
  const { isLoading } = useAuth0();

  if (isLoading) {
    return <Loading />;
  }

  const NavSelect = () => {
    const {isAuthenticated} = useAuth0();
  
    return (
      <nav className= "justify-content-end">
  {isAuthenticated ? <NavLoggedIn /> : <NavLoggedOut />}
      </nav>
    )
  
  }

  return (
    <div id="app" className="d-flex flex-column h-100">
      <NavSelect />
      <Container className="flex-grow-1 mt-5">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/profile" component={Profile} />
          <Route path="/external-api" component={ExternalApi} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/management" {isAuthenticated ?  component={ManagementPage} : ""} />
        </Switch>
      </Container>
      <Footer />
    </div>
  );
};

export default App;