import React, { Component } from "react";

import axios from "axios";

import { Line } from "react-chartjs-2";

//https://dzone.com/articles/create-charts-in-reactjs-using-chartjs

export class Project extends Component {
  constructor(props) {
    super(props);

    this.state = { Data: {} };
  }

  componentDidMount() {
    axios
      .get("https://localhost:5002/api/project")

      .then((res) => {
        console.log(res);

        const ipl = res.data;

        let projectname = [];

        let projectdates = [];

        ipl.forEach((record) => {
          projectname.push(record.projectname);
          projectdates.push(record.projectid);
        });

        this.setState({
          Data: {
            labels: projectname,

            datasets: [
              {
                label: "Project ID",

                data: projectdates,

                backgroundColor: [
                  "#3cb371",

                  "#0000FF",

                  "#9966FF",

                  "#4C4CFF",

                  "#00FFFF",

                  "#f990a7",

                  "#aad2ed",

                  "#FF00FF",

                  "Blue",

                  "Red",
                ],
              },
            ],
          },
        });
      });
  }

  render() {
    return (
      <div>
        <Line data={this.state.Data} options={{ maintainAspectRatio: false }} />
      </div>
    );
  }
}

export default Linecharts;
