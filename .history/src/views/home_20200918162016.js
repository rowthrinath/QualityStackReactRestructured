import React, { Fragment } from "react";
import { Hero, Content } from "../components";
import { Hero1 } from "../components/hero";
import { useAuth0 } from "@auth0/auth0-react";


const HomeSelect = () => {
  const {isAuthenticated} = useAuth0();

  return (
    <nav className= "justify-content-end">
{isAuthenticated ? <Hero /> : <Hero1 />}
    </nav>
  )

}


const Home = () => (
  <Fragment>
    <Hero />
    <hr />
    <Content />
  </Fragment>
);

export default Home;
