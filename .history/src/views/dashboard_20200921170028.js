import React, { Fragment } from "react";
import ProjectChart from "../components/dashboard";
import { Container } from "reactstrap";
import { useAuth0 } from "@auth0/auth0-react";

const Dashboard = () => {
 return (
  {isAuthenticated ? 
 <Fragment>
    <Container id="Container">
      <div className="card">
        <Linecharts />{" "}
      </div>
    </Container>
  </Fragment> :
);
}

export default Dashboard;
