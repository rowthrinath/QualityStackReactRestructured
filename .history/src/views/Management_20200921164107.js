import React, { Fragment, Component } from "react";
import ProjectList from "../components/projectlist";
import ReleaseList from "../components/releaselist";
import "../styles/project.css";
import { Card, Col, Row, Container } from "reactstrap";

export const ManagementPage = () => ( 
    <Fragment>
      <h1 id="h1">Management</h1>
      <div>
        <Row>
          <Col sm={{ size: "auto", offset: 1 }}>
            <ProjectList />
          </Col>
          <Col sm={{ size: "auto", offset: 1 }}>
            <ReleaseList />
          </Col>
        </Row>
      </div>
    </Fragment> 
);

export default ManagementPage;
