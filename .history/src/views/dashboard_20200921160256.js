import React from 'react'
import Linecharts from '../charts/demo'
import { Container } from 'reactstrap'

const Dashboard = () => (
<Container id="Container">
      <div className="card"><Linecharts /> </div>      
      </Container>
)

export default Dashboard