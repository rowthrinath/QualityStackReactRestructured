import React, { Fragment } from "react";
import { Hero, Content } from "../components";
import { LoggedinHome } from "../components/LoggedinHome";
import { useAuth0 } from "@auth0/auth0-react";


const HomeSelect = () => {
  const {isAuthenticated} = useAuth0();

  return (
    <nav className= "justify-content-end">
{isAuthenticated ? <LoggedinHome /> : <Hero />}
    </nav>
  )

}

const ContentSelect = () => {
  const {isAuthenticated} = useAuth0();

  return (
    <nav className= "justify-content-end">
{isAuthenticated ? <Content /> : ""}
    </nav>
  )

}


const Home = () => (
  <Fragment>
    <HomeSelect />
    <hr />
    <Content />
  </Fragment>
);

export default Home;
