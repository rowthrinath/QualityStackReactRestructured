import React, { Fragment } from "react";
import { Hero, Content } from "../components";
import { useAuth0 } from "@auth0/auth0-react";

const HomeSelect = () => {
  const {isAuthenticated} = useAuth0();

  return (
    <nav className= "justify-content-end">
{isAuthenticated ? <NavLoggedIn /> : <NavLoggedOut />}
    </nav>
  )

}


const Home = () => (
  <Fragment>
    <Hero />
    <hr />
    <Content />
  </Fragment>
);

export default Home;
