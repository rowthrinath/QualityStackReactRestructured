import React from "react";
import { NavLink as RouterNavLink } from "react-router-dom";
import { Container, Nav, Navbar } from "react-bootstrap";

import { useAuth0 } from "@auth0/auth0-react";
import LogoutButton from "./logout-button";



const MainNav = () => {
  const {isAuthenticated} = useAuth0();
  return(    
  <Nav className="mr-auto">
    <Nav.Link
      as={RouterNavLink}
      to="/"
      exact
      activeClassName="router-link-exact-active"
    >
      Home
    </Nav.Link>
    <Nav.Link
      as={RouterNavLink}
      to="/profile"
      exact
      activeClassName="router-link-exact-active"
    >
      Profile
    </Nav.Link>
    {isAuthenticated <Nav.Link    
      as={RouterNavLink}
      to="/external-api"
      exact
      activeClassName="router-link-exact-active">
      Docs
    </Nav.Link>
    <Nav.Link
      as={RouterNavLink}
      to="/dashboard"
      exact
      activeClassName="router-link-exact-active">
      Dashboard
    </Nav.Link>
    <Nav.Link    
      as={RouterNavLink}
      to="/management"
      exact
      activeClassName="router-link-exact-active">
      Management
    </Nav.Link>
  </Nav>
)
};

const AuthNav = () => {
  const {isAuthenticated} = useAuth0();

  return (
    <nav className= "justify-content-end">
<LogoutButton />
    </nav>
  )

}

const NavBar = () => {
  return (
    <Navbar bg="light" expand="md">
      <Container>
        <Navbar.Brand as={RouterNavLink} className="logo" to="/" />
        <MainNav />
        <AuthNav />
      </Container>
    </Navbar>
  );
};

export default NavBar;
