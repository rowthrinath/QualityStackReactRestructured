import React from 'react';
import '../../styles/DrawerToggleButton.css';

const drawerToggleButton = props =>(

    <button className = "toggle-button">

    <div className = "toggle-button_line" onClick={props.click}/>
    <div className = "toggle-button_line" onClick={props.click}/>
    <div className = "toggle-button_line" onClick={props.click}/>

    </button>

);

export default drawerToggleButton;