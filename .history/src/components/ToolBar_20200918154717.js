import React from "react";
import "../styles/Sidebar.css";
import "./SideDrawer/DrawerToggleButton";
import DrawerToggleButton from "./SideDrawer/DrawerToggleButton";
import Auth from "./Auth";

// TODO : Implement styling
export const ToolBar = (props) => (
  <header className="toolbar">
    <nav className="toolbar_navigation ">
      <div className="toolbar_toggle_button">
        <DrawerToggleButton click={props.drawerClickHandler} />
      </div>
      <div className="toolbar_logo">
        {" "}
        <a href="/">Quality Stack</a>{" "}
      </div>
      <div className="spacer"></div>
      <div className="toolbar_navigation-items">
        <ul>
          <li>
            <a href="/">Home</a>
          </li>
          {!Auth.authenticated ? (
            <li>
              <a href="/loginpage" name="#loginlink">Login</a>
            </li>
          ) : (
            <li>
              <a href="/">Logout</a>
            </li>
          )}
          <li>
            <a href="/ManagementPage">Register</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
);

export default ToolBar;
