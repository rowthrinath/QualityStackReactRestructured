import React, { Component } from "react";
import axios from "axios";
import {
  Card,
  Row,
  Col,
  Table,
  Container,
  CardHeader,
  Button,
} from "reactstrap";
import "../styles/project.css";

class Management extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
    };
  }

  componentDidMount() {
    axios
      .get("https://localhost:5002/api/project")
      .then((response) => {
        console.log(response);
        this.setState({ projects: response.data });
      })
      .catch((error) => {
        console.log(error);
        //this.startdate({ errorMsg: "Error retreiving data" });
      });
  }

  render() {
    const { projects, errorMsg } = this.state;
    return (
      <div >
        <Container>
          <Row xs="4">
            <Col xs="6">
              <div>
                <Card id="Card">
                  <CardHeader id="CardHeader">Project List</CardHeader>
                  <Table>
                    <thead>
                      <tr>
                        <th>Project Id</th>
                        <th>Project Name</th>
                        <th>Project Start Date</th>
                        <th>Project End Date</th>
                      </tr>
                    </thead>

                    {projects.map((project) => (
                      <tbody>
                        <tr>
                          <th scope="row">{project.projectid}</th>
                          <td>{project.projectname}</td>
                          <td>{project.startdate}</td>
                          <td>{project.enddate}</td>
                        </tr>
                      </tbody>
                    ))}
                  </Table>                  
                </Card>
                <Container>
                {/* <AddProjectModal /> */}
                </Container>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Management;
