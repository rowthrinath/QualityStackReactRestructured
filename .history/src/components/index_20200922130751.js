import Content from "./content";
import Footer from "./footer";
import Hero from "./hero";
import Loading from "./loading";
import NavBar from "./nav-barloggedout";
import Highlight from "./highlight";
importPrivate from 'module'

export { Content, Footer, Hero, Loading, NavBar, Highlight };
