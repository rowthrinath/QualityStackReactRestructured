import React from "react";
import ReactDOM from "react-dom";
import App from "./app";
import { BrowserRouter as Router } from "react-router-dom";
import Auth0ProviderWithHistory from "./auth0-provider-with-history";

import Content from "./content";
import Footer from "./footer";
import Hero from "./hero";
import Loading from "./loading";
import NavBar from "./nav-bar";
import Highlight from "./highlight";
mport PrivateRoute from "./private-route";


export { Content, Footer, Hero, Loading, NavBar, Highlight, PrivateRoute };

import "./index.css";

ReactDOM.render(
  <Router>
    <Auth0ProviderWithHistory>
      <App />
    </Auth0ProviderWithHistory>
  </Router>,
  document.getElementById("root")
);
