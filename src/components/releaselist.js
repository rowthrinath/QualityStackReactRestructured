import React, { Component } from "react";
import axios from "axios";
import { Card, Row, Col, Table, Container, CardHeader, Button } from "reactstrap";
import "../styles/project.css";

class ReleaseList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      releases: [],
    };
  }

  componentDidMount() {
    axios
      .get("https://localhost:5002/api/release")
      .then((response) => {
        console.log(response);
        this.setState({ releases: response.data });
      })
      .catch((error) => {
        console.log(error);
        //this.startdate({ errorMsg: "Error retreiving data" });
      });
  }

  render() {
    const { releases, errorMsg } = this.state;
    return (
      <div style={{ backgroundColor: "", width: "600px", minHeight: "200px" }}>
        <Container>          
          <Row xs="4">
            <Col xs="6">
              <div>
                <Card id="Card">
                  <CardHeader id="CardHeader" >
                    Release List
                  </CardHeader>
                  <Table >
                    <thead>
                      <tr>
                        <th>Release Id</th>
                        <th>Release Name</th>
                        <th>Project Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                      </tr>
                    </thead>

                    {releases.map((release) => (
                      <tbody>
                        <tr>
                          <th scope="row">{release.releaseid}</th>
                          <td>{release.projectname}</td>
                          <td>{release.releasename}</td>
                          <td>{release.releasestartdate}</td>
                          <td>{release.releaseenddate}</td>
                        </tr>
                      </tbody>
                    ))}
                  </Table>
                  <Button >Add</Button>
                </Card>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default ReleaseList;
